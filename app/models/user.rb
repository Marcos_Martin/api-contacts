class User < ApplicationRecord
  has_secure_password
  has_secure_token
  has_many :contacts, dependent: :delete_all # si elimino un usuario, elimino sus contactos
  # Este metodo no esta disponible en has_secure_token
  def invalidate_token
    self.update_columns(token: nil)
  end

  def self.valid_login?(email, password)
    user = find_by(email: email)
    if user && user.authenticate(password)
      user
    end
  end
end
