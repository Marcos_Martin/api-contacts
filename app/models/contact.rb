class Contact < ApplicationRecord
  belongs_to :user
  has_many :phones, dependent: :delete_all # si elimino un contacto, elimino tambien sus telefonos

  has_attached_file :avatar, styles: {original: '300x300>'}, default_url: '/images/no_image.jpg'

  validates_attachment :avatar,
                       content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] },
                       size: { in: 0..1024.kilobytes }
  validates :name, presence: true
  validates :surname, presence: true
  validates :email, presence: true

end
