# He preferido meter todas las acciones de usuario en este controlador para englobor todas las acciones
# que un usuario puede hacer en su agenda. En un futuro tal vez pueda hacer otras cosas disitintas y
# esas funcionalidades irian en otro controlador

# Seria conveniente crear otro controlador de administracion para que un usuario admin pudiera
# tener acceso y control sobre usuarios, contactos y telefonos. Este apartado no se pide en el enunciado

class UsersController < ApiController
  before_action :require_login, except: [:create_user]

  def create_user
    @user = User.new(user_params)

    if @user.save
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: @user.errors}
    end
  end

  def create_contact
    @contact = Contact.new(contact_params)
    @contact.user_id = current_user.id
    if @contact.save
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: @contact.errors}
    end
  end

  def create_phone
    # Pongo mucha atencion en permitir crear telefonos solo para un contacto del usuario identificado
    contact=Contact.find_by_id(params[:contact_id])
    phone = Phone.new(phone_params)
    phone.contact_id = params[:contact_id]
    if contact.user_id==current_user.id && phone.save
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: @phone.errors}
    end
  end
  def delete_phone
    # Pongo mucha atencion en permitir en eliminar telefonos solo para un contacto del usuario identificado
    contact=Contact.find_by_id(params[:contact_id])
    phone = Phone.find_by_id(params[:id])
    phone.contact_id = params[:contact_id]
    if contact.user_id==current_user.id && phone.destroy
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: @phone.errors}
    end
  end

  def contacts
    render json: current_user.contacts.to_json(include: :phones)
  end

  def contacts_by_name_or_surname
    searched=current_user.contacts.where("name LIKE '%#{params[:name]}%' AND
                                          surname LIKE '%#{params[:surname]}%'").to_json(include: :phones)
    render json: searched
  end

  def show_contact
    render json: current_user.contacts.find_by_id(params[:id]).to_json(include: :phones) # me aseguro de mostrar un contacto suyo
  end

  def delete_contact
    # Pongo mucha atencion en permitir borrar solo un contacto del usuario (y sus telefonos asociados)
    finded=Contact.find_by_id(params[:id])
    if current_user.contacts.find_by_id(params[:id]) == finded
      if finded.destroy
        render json: {result: "ok"}
      else
        render json: {result: "error", debug: finded.errors}
      end
    else
      render json: {result: "denied", debug: finded.errors}
    end
  end
  def delete_myself
    user=current_user
    if user.destroy
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: user.errors}
    end
  end
  def update_myself
    #se grabara aunque venga sin contraseña (asi podemos actualizar sin actualizar contraseña)
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end
    myself = current_user
    if myself.update_attributes(user_params)
      render json: {result: "ok"}
    else
      render json: {result: "error", debug: myself.errors}
    end
  end
  def update_contact
    # Pongo mucha atencion en permitir borrar solo un contacto del usuario (y sus telefonos asociados)
    finded=Contact.find_by_id(params[:id])
    if current_user.contacts.find_by_id(params[:id]) == finded
      if finded.update_attributes(contact_params)
        render json: {result: "ok"}
      else
        render json: {result: "error", debug: finded.errors}
      end
    else
      render json: {result: "denied", debug: finded.errors}
    end
  end
  def show_user_by_token
    finded=User.find_by_token(current_user.token)
    if finded == nil
      render json: {result: "error", debug: finded.errors}
    else
      render json: finded
    end
  end
  private
  def user_params
    params.require(:user).permit(:name, :surname, :phone, :email, :rank, :password)
  end

  def contact_params
    params.require(:contact).permit(:name, :surname, :phone, :email, :type, :avatar)
  end

  def phone_params
    params.require(:phone).permit(:number)
  end
end
