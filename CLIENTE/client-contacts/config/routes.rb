Rails.application.routes.draw do
  root "home#index"

  resources :sessions, only: [:new, :create, :destroy]
  resources :contacts, only: [:index, :update, :destroy] do
    delete 'delete_phone/:id(.:format)', to: 'phones#destroy', as: 'delete_phone'
    post 'create_phone(.:format)' => "phones#create", as: 'create_phone'
  end
  resources :users, except: [:show,:index]
end
