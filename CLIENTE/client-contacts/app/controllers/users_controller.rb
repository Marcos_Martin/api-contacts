class UsersController < ApplicationController
  def edit
    ms=HTTParty.get(@api_url+"/user/show_user_by_token.json", headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    @body = JSON.parse(ms.body)
    #binding.pry
  end

  def update
    #curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -X PUT --data "user[name]=ErSevilla&user[surname]=Burbujita&user[phone]=666122312&user[email]=LaSalle@salle.com&user[password]=1234567" http://localhost:3000/user/update_myself.json > result.json
    values={user: {name: params["name"],
                   surname: ["surname"],
                   phone: ["phone"],
                   email: ["email"],
                   email: ["password"]
    }}
    ms=HTTParty.put(@api_url+"/user/update_myself.json", body: values, headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    redirect_to edit_user_path(session[:token]), notice: "Usuario actualizado: #{ms.inspect}"
  end

  def new

  end

  def create

  end

  def destroy

  end
end
