class PhonesController < ApplicationController
  def create
    #curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F "phone[number]=666 555 444"  http://localhost:3000/user/contact/1/create_phone.json > result.json
    values={phone: {number: params["phone"]["number"] }}
    ms=HTTParty.post(@api_url+"/user/contact/#{params[:contact_id]}/create_phone.json",  body: values ,headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    redirect_to contacts_path , notice: "Telf añadido: #{ms.inspect} "
  end
  def destroy
    ms=HTTParty.delete(@api_url+"/user/contact/#{params[:contact_id]}/delete_phone/#{params[:id]}.json", headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    redirect_to contacts_path , notice: "Telf eliminado: #{ms.inspect} "
  end
end
