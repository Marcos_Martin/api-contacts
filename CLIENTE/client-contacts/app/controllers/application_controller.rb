class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :web_default #variables que se inician por defecto

  private
  def web_default
    @api_url="http://localhost:3000"
  end
end
