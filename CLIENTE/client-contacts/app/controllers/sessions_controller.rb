class SessionsController < ApplicationController
  def new
    
  end
  def create
    values={email: params[:email] , password: params[:password]}
    ms=HTTParty.post("http://localhost:3000/login.json", body: values )
    body = JSON.parse(ms.body)
    session[:token]=body["token"]
    redirect_to root_path, notice: "Identicado con token #{body["token"]}"

  end
  def destroy
    session["token"]=nil
    redirect_to root_path, notice: "Cerraste la sesión"
  end
end
