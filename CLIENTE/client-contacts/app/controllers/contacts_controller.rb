class ContactsController < ApplicationController
  def index
    ms=HTTParty.get(@api_url+"/user/contacts.json", headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    @body = JSON.parse(ms.body)
  end
  def update
    values={contact: {name: params["contact"]["name"],
              surname: params["contact"]["surname"],
              phone: params["contact"]["phone"],
              email: params["contact"]["email"]
    }}
    ms=HTTParty.put(@api_url+"/user/update_contact/#{params["id"]}.json",  body: values ,headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    redirect_to contacts_path , notice: "Contacto actualizado: #{ms.inspect} "
  end
  def destroy
    ms=HTTParty.delete(@api_url+"/user/delete_contact/#{params["id"]}.json", headers: {"Authorization" => "Token token=\"#{session[:token]}\""})
    redirect_to contacts_path , notice: "Contacto eliminado: #{ms.inspect} "
  end
end
