# REQUISITOS DE API CONTACTS

1. La aplicación debe tener usuarios que puedan guardar sus contactos.

2. Cada contacto tiene que tener obligatoriamente nombre, apellidos e email donde el email tiene que ser único.  
El contacto puede tener una foto y varios teléfonos.  
Los contactos pueden ser de tres tipos: amigos, familiares y compañeros.

3. Vía API se podrán realizar las siguientes operaciones  
  a) Ver todos los contactos de un usuario. Se deberán poder filtrar por nombre y apellido.  
  b) Ver los detalles de un contacto de cualquier usuario.  
  c) Crear, actualizar y borrar usuarios (las dos ultimas requieren autentificacion de dicho usuario)  
  d) Crear, actualizar y borrar contactos de un usuario (para un usuario autentificado)  
  e) Generar un token de autorización para un usuario, que permita autorizar sus peticiones sin necesidad de usar sus credenciales (user,password) (opcional)  

4. El cliente tendrá las siguientes características (opcional)  
   a) Debe poder realizar las anteriores operaciones, autentificando   al usuario mediante credenciales o token.  
   b) Dispondrá de una interfaz gráfica sencilla para realizar las operaciones y mostrar los resultados.  


# LLAMADAS A LA API DE ARQUITECTURA REST (**SERVIDOR**)

* Conseguir token

```
#!

curl -X POST --data "email=xerif@foo.com&password=123456" http://localhost:3000/login.json
```


* Registrar nuevo usuario

```
#!

curl -X POST --data "user[name]=ErSevilla&user[surname]=Burbujita&user[phone]=666122312&user[email]=LaSalle@salle.com&user[password]=1234567" http://localhost:3000/user/create_user.json > result.json
```


* Todos mis contactos

```
#!

curl -H "Authorization: Token token=vTYRqAWQmkWY2aLFhfo3notQ" http://localhost:3000/user/contacts.json
```


* Añadir nuevo contacto

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F contact[avatar]=@app/images/scully.png -F "contact[name]=Dana" -F "contact[email]=fooScully@address.com" -F "contact[surname]=Scully" -F "contact[phone]=669123123" -F "contact[type]=Family" http://localhost:3000/user/create_contact.json > result.json
```


* Buscar contacto por nombre y/o apellido (puedes dejar name o surname vacios)

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F "name=Dana" -F "surname=Scully" -X GET http://localhost:3000/user/contacts_by_name_or_surname.json > result.json
```


* Mostrar un contacto (y sus teléfonos)

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F "id=2" -X GET http://localhost:3000/user/show_contact.json > result.json
```


* Eliminar contacto

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -X DELETE http://localhost:3000/user/delete_contact/20.json > result.json
```


* Eliminar usuario (a si mismo)

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -X DELETE http://localhost:3000/user/delete_myself.json > result.json
```


* Modificar usuario

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -X PUT --data "user[name]=ErSevilla&user[surname]=Burbujita&user[phone]=666122312&user[email]=LaSalle@salle.com&user[password]=1234567" http://localhost:3000/user/update_myself.json > result.json
```


* Modificar contacto

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F contact[avatar]=@app/images/scully.png -F "email=fooScully@address.com" -F "contact[name]=Dana" -F "email=fooScully@address.com" -F "contact[surname]=Scully" -F "contact[phone]=669123123" -F "contact[type]=Family" -X PUT http://localhost:3000/user/update_contact/20.json > result.json
```


* Crear teléfono para un contacto (este teléfono debe ser para un contacto del usuario identificado)

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -F "phone[number]=666 555 444"  http://localhost:3000/user/contact/1/create_phone.json > result.json
```


* Eliminar teléfono de un contacto (este teléfono debe ser para un contacto del usuario identificado)

```
#!

curl -H "Authorization: Token token=tUsFpXULLYL7LNzwcK88FYW7" -X DELETE http://localhost:3000/user/contact/1/delete_phone/90.json > result.json
```


* Mostrar usuario por token
```
#!
curl -H "Authorization: Token token=KedMH6SWLBygDcGg7ccsMWQb" -X GET http://localhost:3000/user/show_user_by_token.json > result.json
```


# Herramientas para el servidor

* Resetear base de datos y sembrarla

```
#!

rake database:reset
```


* Poblar base de datos con datos de prueba

```
#!

rake database:seed_fakes
```


* Ver/analizar resultados formateados con un navegador web.
Para cada llamada curl, terminarlas con el redirector "> result.json" y abrir result.json con un navegador web, ej:

```
#!

curl -H "Authorization: Token token=Qtg6tHyjWWAc584LhLNAooSB" http://localhost:3000/user/contacts.json > result.json
```


# Test del servidor

* Ejecutar tests


```
#!

bundle exec rspec spec/api/users_controller_spec.rb -fd
```

```
#!

bundle exec rspec spec/models/contact_spec.rb -fd
```
* Todos los tests juntos

```
#!
bundle exec rspec spec -fd
```

# CLIENTE DE PRUEBAS
El cliente se encuentra en el directorio CLIENTE del proyecto.
El servidor arrancaría desde el puerto 3000 y el cliente desde el 3001 (ya están configurados así en desarrollo)
El cliente es una **versión alpha de pruebas** con una arquitectura provisional y un funcionamiento que
en ocasiones puede ser inestable. Todas las funcionalidades del servdor no están implementadas por ser
repetitivas y por cuestiones de tiempo/trabajo. No hay test para el cliente
Enjoy testing!
