Rails.application.routes.draw do
  # Identificacion de usuarios
  scope :format => true, :constraints => { :format => 'json' } do
    post   "/login"       => "sessions#create"
    delete "/logout"      => "sessions#destroy"
  end

  #-------- Llamadas a la api ---------
  # Muestrame mis contactos
  get 'user/contacts' => "users#contacts"
  # Crear nuevo contacto para el usuario identificado
  post 'user/create_contact' => "users#create_contact"
  # Crear nuevo usuario
  post 'user/create_user' => "users#create_user"
  # Buscar contactos por nombre y/o apellido
  get 'user/contacts_by_name_or_surname' => "users#contacts_by_name_or_surname", as: 'contacts_by_name_or_surname'
  # Buscar un solo contacto
  get 'user/show_contact' => "users#show_contact", as: 'show_contact'
  # Eliminar contacto
  delete 'user/delete_contact/:id(.:format)', to: 'users#delete_contact', as: 'delete_contact'
  # Eliminar usuario (a si mismo)
  delete 'user/delete_myself(.:format)', to: 'users#delete_myself', as: 'delete_myself'
  # Modificar usuario (a si mismo)
  put 'user/update_myself(.:format)', to: 'users#update_myself', as: 'update_myself'
  # Modificar contacto (del usuario)
  put 'user/update_contact/:id(.:format)', to: 'users#update_contact', as: 'update_contact'
  # Crear nuevo telefono para un contacto del usuario identificado
  post 'user/contact/:contact_id/create_phone(.:format)' => "users#create_phone", as: 'create_phone'
  # Eliminar telefono para de un contacto del usuario identificado
  delete 'user/contact/:contact_id/delete_phone/:id(.:format)', to: 'users#delete_phone', as: 'delete_phone'
  # Mostrar un usuario por token
  get 'user/show_user_by_token(.:format)' => "users#show_user_by_token", as: 'show_user_by_token'
end
