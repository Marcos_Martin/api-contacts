require 'spec_helper'
require 'rails_helper'

describe UsersController do
  before(:each) do

  end
  after(:each) do

  end


  describe 'GET contacts: /user/contacts' do
    before(:each) do
      @contact = Fabricate(:contact)
      @token=@contact.user.token
      get "/user/contacts", params: {}, headers: { "Authorization" => "Token #{@token}" }
    end

    it 'responds with 200' do
      expect(response.status).to eq(200)
    end
  end

  describe 'POST user: user/create_user' do
    it 'Saves the request user in the database (Deprecate mode on!)' do
      expect {
      post "/user/create_user", user: {name: 'Michael', surname: 'Knight', phone: '666 22 33 11',
                                        email:'bugs@kitt.com',password: '12345678'}
      }.to change(User, :count).by(1)
    end
  end
  describe 'POST contact: user/create_contact' do
    before(:each) do
      contact = Fabricate(:user)
      @token= contact.token
    end
    it 'Save contact with respond = ok' do
      post "/user/create_contact", params: {contact: {name: 'Kitt',
                                                      surname: 'Knight Industries Two Thousand',
                                                      phone: '666 22 33 11',
                                                      email:'bugs@kitt.com',
                                                      type: 'Family',
                                                      }},
           headers: { "Authorization" => "Token #{@token}" }
      parse_json = JSON(response.body)
      expect(parse_json["result"]).to eq('ok') # passes if actual == expected
    end
  end
  describe 'DELETE contact: user/delete_contact/:id(.:format)' do
    before(:each) do
      @contact = Fabricate(:contact)
      @token=@contact.user.token
    end
    it 'Delete contact' do
      expect {
      delete "/user/delete_contact/#{@contact.id}", params: {}, headers: { "Authorization" => "Token #{@token}" }
      }.to change(Contact, :count).by(-1)
    end
  end
  describe 'PUT contact: user/update_contact/:id(.:format)' do
    before(:each) do
      @contact = Fabricate(:contact)
      @token= @contact.user.token
    end
    it 'Update contact in database' do
      put "/user/update_contact/#{@contact.id}", params: {contact: {name: 'Karr',
                                                      surname: 'Knight Industries Two Thousand',
                                                      phone: '666 22 33 12',
                                                      email:'bugs@karr.com',
                                                      type: 'Friend',
      }},
           headers: { "Authorization" => "Token #{@token}" }
      @contact=Contact.last
      expect(@contact.name).to eq('Karr')
    end
  end

end