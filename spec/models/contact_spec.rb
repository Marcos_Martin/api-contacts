require 'spec_helper'
require 'rails_helper'

describe Contact do
  it 'has a valid factory' do
    contact = Fabricate(:contact)
    expect(contact).to be_valid
  end
  it 'is invalid without a user' do
    contact = Fabricate.build(:contact, user: nil)
    expect(contact).to_not be_valid
  end
end