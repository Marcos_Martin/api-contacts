Fabricator(:contact) do
  user     { Fabricate(:user)  }
  name     'Kitt'
  surname  'Knight Industries Two Thousand'
  phone    '666 22 33 11'
  email    'bugs@pontiac.com'
  type     'Family'
  avatar    File.new("#{Rails.root}/app/images/kitt.jpg")
end