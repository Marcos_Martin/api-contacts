class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.integer :user_id
      t.string :name
      t.string :surname
      t.string :phone # este es el teléfono principal
      t.string :email
      t.string :type
      t.string :avatar

      t.timestamps
    end
    add_attachment :contacts, :avatar
    add_foreign_key "contacts", "phones", on_delete: :cascade
  end
end
