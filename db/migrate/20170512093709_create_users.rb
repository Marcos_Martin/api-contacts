class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :phone
      t.string :token
      t.string :email
      t.string :rank, default: 'customer'  # el usuario puede cambiar y ser "admin"
      t.string :password_digest

      t.timestamps
    end
    add_index :users, :token
    add_index :users, :email, :unique=> true
    add_index :users, :name
    add_index :users, :surname
    add_foreign_key "users", "contacts", on_delete: :cascade
  end
end
