namespace :database do
  desc "TODO"
  task :reset => :environment do
    File.delete("#{Rails.root}/db/schema.rb")
    FileUtils.rm_rf("#{Rails.root}/public/system") # borrado de archivos adjuntos
    Rake::Task["db:migrate:reset"].invoke
    p "--------------RESET OK"
    Rake::Task["db:seed"].invoke
    p "--------------SEED OK"
  end
  task :seed_fakes => :environment do
    for i in 0..3 # creamos usuarios clientes de prueba
      User.create(
          name: Faker::Name.first_name,
          surname: Faker::Name.last_name+' '+Faker::Name.last_name,
          phone: Faker::PhoneNumber.phone_number,
          email: "foo#{i}@foo.com",
          password: "123456#{i}")
    end
    p "--------------creados #{i} usuarios de prueba"

    sampleImages=["arrow.png","doctorWho.jpg","princesaAmidala.jpg","scully.png"]
    User.all.each do |user| # le damos a cada usuario unos cuantos contactos
      for i in 0..(rand 1..5)
        Contact.create(user_id: user.id, name: Faker::Name.first_name,
                       surname: Faker::Name.last_name+' '+Faker::Name.last_name,
                       phone: Faker::PhoneNumber.phone_number,
                       email: "contactfoo#{i}@foo.com",
                       type: [:Partner, :Friend, :Family].sample,
                       avatar: File.new("#{Rails.root}/app/images/#{sampleImages.sample}"))
      end
    end
    p "--------------creados contactos aleatorios para los usuarios de prueba"


    Contact.all.each do |contact| # le damos a cada contacto unos cuantos telefonos extra
      for i in 0..(rand 1..5)
        Phone.create(contact_id: contact.id, number: Faker::PhoneNumber.phone_number)
      end
    end
    p "--------------creados telefonos aleatorios extra para los contactos de prueba"

  end
end